Name:		pktgen-dpdk
Version:	3.5.9
Release:	0%{?dist}
Summary:	Traffic generator utilizing DPDK

Group:		Applications/Internet
License:	BSD
URL:		https://github.com/Pktgen/Pktgen-DPDK/
Source:		https://dpdk.org/browse/apps/pktgen-dpdk/snapshot/pktgen-%{version}.tar.gz
Patch:		use-pkg-config-for-lua.patch

BuildRequires:	gcc sed wdiff
BuildRequires:	dpdk-devel >= 18.11
# bogus deps due to makefile confusion over static linkage and whatnot
BuildRequires:	libpcap-devel zlib-devel numactl-devel
BuildRequires:	openssl-devel
# RHEL7 doesn't have LUA 5.3
%if 0%{?rhel} && 0%{?rhel} < 8
BuildRequires:	lua53-devel
%else
BuildRequires:	lua-devel
%endif
BuildRequires:	python2-sphinx

%description
%{summary}

%prep
%autosetup -n pktgen-%{version} -p1
# Remove builtin RPATHS
sed -i '/^\s*RPATHS += /d' app/Makefile
# On Fedora/EPEL lua installs stuff as lua instead of lua5.3
sed -i 's/lua5\.3/lua/g' app/Makefile lib/lua/Makefile
# Strip Carriage Returns
sed -i 's/\r$//' test/*.txt

%build
unset RTE_SDK
. /etc/profile.d/dpdk-sdk-%{_arch}.sh

# Copied from dpdk.spec
# Avoid appending second -Wall to everything, it breaks upstream warning
# disablers in makefiles. Strip expclit -march= from optflags since they
# will only guarantee build failures, DPDK is picky with that.
# Note: _hardening_ldflags has to go on the extra cflags line because dpdk is
# astoundingly convoluted in how it processes its linker flags.  Fixing it in
# dpdk is the preferred solution, but adjusting to allow a gcc option in the
# ldflags, even when gcc is used as the linker, requires large tree-wide changes
touch obj.o
gcc -### obj.o 2>&1 | awk '/.*collect2.*/ { print $0}' > ./noopts.txt
gcc -### $(rpm --eval '%{build_ldflags}') obj.o 2>&1 | awk '/.*collect2.*/ {print $0}' > ./opts.txt
EXTRA_RPM_LDFLAGS=$(wdiff -3 -n ./noopts.txt ./opts.txt | sed -e"/^=\+$/d" -e"/^.*\.res.*/d" -e"s/\[-//g" -e"s/\-\]//g" -e"s/{+//g" -e"s/+}//g" -e"s/\/.*\.o //g" -e"s/ \/.*\.o$//g" -e"s/-z .*//g" | tr '\n' ' ')
rm -f obj.o

export EXTRA_CFLAGS="$(echo %{optflags} | sed -e 's:-Wall::g' -e 's:-march=[[:alnum:]]* ::g') -Wformat -fPIC %{_hardening_ldflags}"
%if 0%{?fedora} > 27 || 0%{?rhel} > 7
%ifarch x86_64 i686
export EXTRA_CFLAGS="$EXTRA_CFLAGS -fcf-protection=full"
%endif
%endif
export EXTRA_LDFLAGS=$(echo %{__global_ldflags} | sed -e's/-Wl,//g' -e's/-spec.*//')
export HOST_EXTRA_CFLAGS="$EXTRA_CFLAGS $EXTRA_RPM_LDFLAGS"
export EXTRA_HOST_LDFLAGS=$(echo %{__global_ldflags} | sed -e's/-spec.*//')

%if 0%{?rhel} && 0%{?rhel} < 8
export PKG_CONFIG_PATH="${PKG_CONFIG_PATH:+$PKG_CONFIG_PATH:}/opt/lua-5.3/lib64/pkgconfig"
%endif

make V=1 %{?_smp_mflags} RPATHS="-rpath=%{_libdir}/%{name}"

make -C docs/ V=1 man %{?_smp_mflags}

%install
unset RTE_SDK
. /etc/profile.d/dpdk-sdk-%{_arch}.sh

# No working "install" target, lets do it manually (sigh)
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_libdir}/%{name}
mkdir -p %{buildroot}%{_mandir}/man1
if [ -f app/${RTE_TARGET}/pktgen ]; then
  install -m 755 app/${RTE_TARGET}/pktgen %{buildroot}%{_bindir}/pktgen
else
  install -m 755 app/app/${RTE_TARGET}/pktgen %{buildroot}%{_bindir}/pktgen
fi
for f in Pktgen.lua PktgenGUI.lua; do
   install -m 644 ${f} %{buildroot}%{_libdir}/%{name}/${f}
done
for f in lib/*/x86_64-default-linuxapp-gcc/lib/*.so.1; do
   install -m 755 ${f} %{buildroot}%{_libdir}/%{name}/
done
install -m 644 docs/build/man/pktgen.1 %{buildroot}%{_mandir}/man1/pktgen.1

%files
%license LICENSE
%doc README.md
%doc pcap themes test
%{_bindir}/pktgen
%{_libdir}/%{name}
%{_mandir}/man1/pktgen.1*

%changelog
* Tue Dec 04 2018 Timothy Redaelli <tredaelli@redhat.com> - 3.5.9-0
- Update to 3.5.9 linked with DPDK 18.11

* Mon Mar 05 2018 Timothy Redaelli <tredaelli@redhat.com> - 3.4.9-0
- Update to 3.4.9 linked with DPDK 18.02

* Fri Jan 12 2018 Timothy Redaelli <tredaelli@redhat.com> - 3.4.8-0
- Update to 3.4.8 linked with DPDK 17.11

* Fri Nov 17 2017 Timothy Redaelli <tredaelli@redhat.com> - 3.4.2-0.1
- Update to 3.4.2 linked with DPDK 17.11

* Fri Jul 28 2017 Timothy Redaelli <tredaelli@redhat.com> - 3.3.8-0
- Update to 3.3.8
- Build manpage

* Fri Mar 03 2017 Flavio Leitner <fbl@redhat.com> - 3.1.2-1
- Update to 3.1.2

* Mon Jan 23 2017 Timothy Redaelli <tredaelli@redhat.com> 3.1.0-1
- Update to 3.1.0
- Bundled lua version is 5.3.3

* Thu Aug 18 2016 Panu Matilainen <pmatilai@redhat.com> - 3.0.12-1
- Update to 3.0.12
- Source name changing yet again, flip-flop SIGH

* Wed Aug 03 2016 Panu Matilainen <pmatilai@redhat.com> - 3.0.09-1
- Update to 3.0.09
- Source name changing yet again, sigh
- Buildrequire openssl-devel

* Thu Apr 28 2016 Panu Matilainen <pmatilai@redhat.com> - 3.0.02-1
- Update to 3.0.02

* Thu Apr 28 2016 Panu Matilainen <pmatilai@redhat.com> - 3.0.0-1
- Update to 3.0.0

* Tue Apr 12 2016 Panu Matilainen <pmatilai@redhat.com> - 2.9.16-1
- Update to 2.9.16

* Mon Apr 04 2016 Panu Matilainen <pmatilai@redhat.com> - 2.9.13-1
- Update to 2.9.13

* Wed Mar 30 2016 Panu Matilainen <pmatilai@redhat.com> - 2.9.12-6
- Bump + rebuild

* Fri Mar 11 2016 Panu Matilainen <pmatilai@redhat.com> - 2.9.12-5
- Hack -march out of rpm optflags to permit build on i686

* Fri Mar 11 2016 Panu Matilainen <pmatilai@redhat.com> - 2.9.12-4
- Bump + rebuild for new arch

* Thu Mar 10 2016 Panu Matilainen <pmatilai@redhat.com> - 2.9.12-3
- Bump + rebuild for ABI changes

* Mon Feb 22 2016 Panu Matilainen <pmatilai@redhat.com> - 2.9.12-2
- Bump + rebuild

* Tue Feb 16 2016 Panu Matilainen <pmatilai@redhat.com> - 2.9.12-1
- Update to 2.9.12

* Wed Jan 20 2016 Panu Matilainen <pmatilai@redhat.com> - 2.9.8-1
- Update to 2.9.8
- Bump DPDK requirement to 2.1

* Wed Dec 16 2015 Panu Matilainen <pmatilai@redhat.com> - 2.9.7-1
- Update to 2.9.7
- Build conflicts with lua-devel, sigh

* Mon Sep 28 2015 Panu Matilainen <pmatilai@redhat.com> - 2.9.2-2
- Rebuild for dpdk changes
- Buildrequires zlib-devel (static linking brokenness carried over
  to shared libraries, sigh)

* Mon Sep 28 2015 Panu Matilainen <pmatilai@redhat.com> - 2.9.2-1
- Update to 2.9.2
- Drop bogus fuse-devel buildreq

* Fri Mar 27 2015 Panu Matilainen <pmatilai@redhat.com> - 2.8.4-1
- Update to 2.8.4

* Mon Mar 02 2015 Panu Matilainen <pmatilai@redhat.com> - 2.8.3-1
- Update to 2.8.3
- Use rpm optflags for building

* Fri Feb 27 2015 Panu Matilainen <pmatilai@redhat.com> - 2.8.0-6
- Rebuild

* Tue Feb 17 2015 Panu Matilainen <pmatilai@redhat.com> - 2.8.0-5
- Add some missing Lua bits and pieces
- Workaround DPDK linking madness by buildrequiring fuse-devel

* Thu Feb 05 2015 Panu Matilainen <pmatilai@redhat.com> - 2.8.0-4
- Another rebuild for versioning change

* Thu Feb 05 2015 Panu Matilainen <pmatilai@redhat.com> - 2.8.0-3
- Another rebuild for versioning change

* Tue Feb 03 2015 Panu Matilainen <pmatilai@redhat.com> - 2.8.0-2
- Rebuild with library versioned dpdk
- Ensure RTE_SDK from dpdk-devel gets used

* Fri Jan 30 2015 Panu Matilainen <pmatilai@redhat.com> - 2.8.0-1
- Initial packaging

